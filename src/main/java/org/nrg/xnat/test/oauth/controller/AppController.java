package org.nrg.xnat.test.oauth.controller;


import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AppController {
	
	private InMemoryClientRegistrationRepository clientRegistrationRepository;
	
	public AppController(InMemoryClientRegistrationRepository clientRegistrationRepository) {
		this.clientRegistrationRepository = clientRegistrationRepository;
	}
	
    @GetMapping("/")
    public ModelAndView home(OAuth2AuthenticationToken token) {
    	System.out.println(token);
        ModelAndView mav = new ModelAndView();
        mav.addObject("repo", clientRegistrationRepository);
        mav.addObject("token", token);
        mav.setViewName("home");
        return mav;
    }
}
