package org.nrg.xnat.test.oauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OidcPkceValidationApplication {

	public static void main(String[] args) {
		SpringApplication.run(OidcPkceValidationApplication.class, args);
	}

}
